﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmallTasks;

namespace TestSmallTasks
{
    [TestClass]
    public class TestPow
    {
        [TestMethod]
        public void Pow_WhenBIsZero_ReturnOne()
        {
            int a = 5;
            int b = 0;
            int expected = 1;
            long actual = NumberTasks.Pow(a, b);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Pow_ReturnsCorrectDegrees()
        {
            int a = 2;
            int b = 3;
            int expected = 8;
            long actual = NumberTasks.Pow(a, b);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Pow_WhenBIsEven()
        {
            int a = 2;
            int b = 4;
            int expected = 16;
            long actual = NumberTasks.Pow(a, b);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Pow_WhenBIsOdd()
        {
            int a = 3;
            int b = 3;
            int expected = 27;
            long actual = NumberTasks.Pow(a, b);
            Assert.AreEqual(expected, actual);
        }
    }
}
