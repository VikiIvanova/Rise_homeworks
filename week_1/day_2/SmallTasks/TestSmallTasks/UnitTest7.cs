﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmallTasks;

namespace TestSmallTasks
{
    [TestClass]
    public class TestGetAverage
    {
        [TestMethod]
        public void GetAverage_WhenArrayIsCorrect()
        {
            int[] array = { 1, 2, 3, 4, 5, 6, 7, 8 };
            double expected = 4.5;
            double actual = NumberTasks.GetAverage(array);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetAverage_WhenSizeOfArrayIsZero()
        {
            int[] array = { };
            Assert.ThrowsException<ArgumentException>(() => NumberTasks.GetAverage(array));
        }

        [TestMethod]
        public void GetAverage_WhenSizeOfArrayIsOne()
        {
            int[] array = { 5 };
            double expected = 5;
            double actual = NumberTasks.GetAverage(array);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetAverage_WhenElementsInArrayIsNegative()
        {
            int[] array = { -9, -5, -6, -2, 3 };
            double expected = -3.8;
            double actual = NumberTasks.GetAverage(array);
            Assert.AreEqual(expected, actual);
        }
    }
}
