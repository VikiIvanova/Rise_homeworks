
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmallTasks;
namespace TestSmallTasks
{
    [TestClass]
    public class TestIsOdd
    {
        [TestMethod]
        public void IsOdd_WhenNumberIsEven_ReturnsFalse()
        {
            // Arrange
            var program = new NumberTasks();

            // Act
            var result = program.IsOdd(2);

            // Assert
            Assert.IsFalse(result);
        }
        [TestMethod]
        public void IsOdd_WhenNumberIsOdd_ReturnsTrue()
        {
            // Arrange
            var program = new NumberTasks();

            // Act
            var result = program.IsOdd(3);

            // Assert
            Assert.IsTrue(result);
        }
    }

    
}



