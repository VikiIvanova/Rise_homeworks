﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmallTasksString;

namespace TestSmallTasks
{
    [TestClass]
    public class UnitTestReverse
    {
        [TestMethod]
        public void TestReverseGivenFiveLetterWord()
        {
            //Average
            string input = "Hello";
            string expected = "olleH";

            //Act
            string actual = SmallTasksString.Reverse(input);

            //Assert
            Assert.AreEqual(expected, actual);

        }
    }
}
