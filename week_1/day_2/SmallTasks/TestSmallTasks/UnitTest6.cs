﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmallTasks;

namespace TestSmallTasks
{
    [TestClass]
    public class TestGetOddOccurrence
    {
        [TestMethod]
        public void GetOddOccurrence_WhenArrayIsCorrect()
        {
            int[] array = { 2, 3, 5, 4, 5, 2, 4, 3, 5, 2, 4, 4, 2 };
            int expected = 5;
            int actual = NumberTasks.GetOddOccurrence(array);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetOddOccurrence_WhenInArrayNotANumberWithOddOccurence()
        {
            int[] array = { 2, 2, 3, 3, 4, 4 };
            int expected = -1;
            int actual = NumberTasks.GetOddOccurrence(array);

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void GetOddOccurance_WhenSizeOfArrayIsZero()
        {
            int[] array = { };
            int expected = -1;
            int actual = NumberTasks.GetOddOccurrence(array);

            Assert.AreEqual(expected, actual);
        }
    }
}
