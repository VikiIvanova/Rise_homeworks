//using Array;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//namespace UnitTestsArrays
//{
//    [TestClass]
//    public class TestFindMissingNumber
//    {
//        [TestMethod]
//        public void FindMissingNumber_WhenInputIsCorrect()
//        {
//            int[] array = new int[] { 2, 3, 5, 1 };
//            int expected = 4;
//            int actual = ArrayTasks.FindMissingNumber(array);
//            Assert.AreEqual(expected, actual);
//        }

//        [TestMethod]
//        public void FindMissingNumber_WhenSizeOnArrayIsZero()
//        {
//            int[] array = { };
//            Assert.ThrowsException<ArgumentException>(() => ArrayTasks.FindMissingNumber(array));
//        }

//        [TestMethod]
//        public void FindMissingNumber_WhenInputIsNotCorrect()
//        {
//            int[] array = { 2, 2, 3, 5 };
//            Assert.ThrowsException<ArgumentException>(() => ArrayTasks.FindMissingNumber(array));
//        }

//        [TestMethod]
//        public void FindMissingNumber_WhenArrayWithNegativeElement()
//        {
//            int[] array = new int[] { -5, -1, -2, -3 };
//            int expected = -4;
//            int actual = ArrayTasks.FindMissingNumber(array);
//            Assert.AreEqual(expected, actual);
//        }

//        [TestMethod]
//        public void FindMissingNumber_WhenSizeOnArrayIsOne()
//        {
//            int[] array = { 5 };
//            Assert.ThrowsException<ArgumentException>(() => ArrayTasks.FindMissingNumber(array));
//        }
//        [TestMethod]
//        public void FindMissingNumberWithSum_WhenInputIsCorrect()
//        {
//            int[] array = new int[] { 2, 3, 5, 1 };
//            int expected = 4;
//            int actual = ArrayTasks.FindMissingNumberWithSum(array);
//            Assert.AreEqual(expected, actual);
//        }

//        [TestMethod]
//        public void FindMissingNumberWithSum_WhenSizeOnArrayIsZero()
//        {
//            int[] array = { };
//            Assert.ThrowsException<ArgumentException>(() => ArrayTasks.FindMissingNumberWithSum(array));
//        }

//        [TestMethod]
//        public void FindMissingNumberWithSum_WhenSizeOnArrayIsOne()
//        {
//            int[] array = { 5 };
//            Assert.ThrowsException<ArgumentException>(() => ArrayTasks.FindMissingNumberWithSum(array));
//        }

//        [TestMethod]
//        public void FindMissingNumberWithSum_WhenArrayWithNegativeElement()
//        {
//            int[] array = new int[] { -5, -1, -2, -3 };
//            int expected = -4;
//            int actual = ArrayTasks.FindMissingNumberWithSum(array);
//            Assert.AreEqual(expected, actual);
//        }

//    }
//}