using Microsoft.VisualStudio.TestTools.UnitTesting;
using Array;
namespace UnitTestsArrays
{
    [TestClass]
    public class TestCubeRoot
    {
        [TestMethod]
        public void TestFindRoot3()
        {
            Assert.AreEqual(2, CubeRoot.FindRoot3(8));
            Assert.AreEqual(1, CubeRoot.FindRoot3(1));
            Assert.AreEqual(3, CubeRoot.FindRoot3(27));
            Assert.AreEqual(0, CubeRoot.FindRoot3(0));
            Assert.AreEqual(14, CubeRoot.FindRoot3(2744));
        }

        [TestMethod]
        public void TestFindCubeRoot()
        {
            Assert.AreEqual(2, CubeRoot.FindCubeRoot(8));
            Assert.AreEqual(1, CubeRoot.FindCubeRoot(1));
            Assert.AreEqual(3, CubeRoot.FindCubeRoot(27));
            Assert.AreEqual(0, CubeRoot.FindCubeRoot(0));
            Assert.AreEqual(14, CubeRoot.FindCubeRoot(2744));
        }

    }
}