﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace Array
{
    public class CubeRoot
    {
        //First solution
        public static int smallestDivisor(int n)
        {
            if (n % 2 == 0)
            {
                return 2;
            }
            for (int i = 3; i * i <= n; i += 2)
            {
                if (n % i == 0)
                {
                    return i;
                }
            }
            return n;
        }

        public static int FindRoot3(int x)
        {
            if (x == 0 || x == 1)
            {
                return x;
            }
            int divisor = 1;
            int cubeRoot = 1;
            while (x > 1)
            {
                int factor = smallestDivisor(x);
                int count = 0;
                while (x % factor == 0)
                {
                    x /= factor;
                    count++;
                }
                if (count % 3 != 0)
                {
                    return 0;
                }
                count /= 3;
                for (int i = 0; i < count; i++)
                {
                    cubeRoot *= factor;
                }
                divisor *= factor;
            }
            if (divisor == cubeRoot)
            {
                return cubeRoot;
            }
            return 0;
        }

        //Second solution
        public static int FindCubeRoot(int x)
        {
            int result = (int)Math.Ceiling(Math.Pow(x, (double)1 / 3));
            return result;
        }

        //Third solution
        //public static int MyPow(int x, double n)
        //{
        //    if (n == 0) return 1;
        //    if (x == 0) return 0;
        //    if (x == 1) return x;

        //    long absN = (long)Math.Abs(n);
        //    double result = 1.0;
        //    double baseValue = x;

        //    while (absN > 0)
        //    {
        //        if (absN % 2 == 1)
        //        {
        //            result *= baseValue;
        //        }

        //        absN /= 2;
        //        baseValue *= baseValue;
        //    }

        //    if (n > 0)
        //    {
        //        return (int)Math.Round(result);
        //    }
        //    else
        //    {
        //        return (int)Math.Round(1.0 / result);
        //    }
        //}
        //public static int FindRoot3(int x)
        //{

        //    int result = MyPow(x, (double)1.0 / 3.0);
        //    return result;
        //}
    }
}
