﻿namespace Array
{
    public class ArrayTasks
    {
        private static void MergeArray(int[] array, int left, int middle, int right)
        {
            var leftArrayLength = middle - left + 1;
            var rightArrayLenght = right - middle;
            var leftTempArray = new int[leftArrayLength];
            var rightTempArray = new int[rightArrayLenght];
            int i, j;

            for (i = 0; i < leftArrayLength; ++i)
            {
                leftTempArray[i] = array[left + i];
            }
            for (j = 0; j < rightArrayLenght; ++j)
            {
                rightTempArray[j] = array[middle + 1 + j];
            }

            i = 0;
            j = 0;
            int k = left;

            while (i < leftArrayLength && j < rightArrayLenght)
            {
                if (leftTempArray[i] <= rightTempArray[j])
                {
                    array[k++] = leftTempArray[i++];
                }
                else
                {
                    array[k++] = rightTempArray[j++];
                }
            }
            while (i < leftArrayLength)
            {
                array[k++] = leftTempArray[i++];
            }
            while (j < rightArrayLenght)
            {
                array[k++] = rightTempArray[j++];
            }
        }
        public static int[] SortArray(int[] array, int left, int right)
        {
            if (left < right)
            {
                int middle = left + (right - left) / 2;
                SortArray(array, left, middle);
                SortArray(array, middle + 1, right);

                MergeArray(array, left, middle, right);
            }
            return array;
        }

        public static bool isCorrectInput(int[] sortedArray)
        {
            for (int i = 0; i < sortedArray.Length - 1; i++)
            {
                if (sortedArray[i] == sortedArray[i + 1])
                {
                    return false;
                }
            }
            return true;
        }

        public static int FindMissingNumber(int[] array)
        {
            if (array.Length == 0 || array.Length == 1)
            {
                throw new ArgumentException("Size of array is zero");
            }
            int[] sortArray = SortArray(array, 0, array.Length - 1);

            if (!isCorrectInput(sortArray))
            {
                throw new ArgumentException("Elements in array is not correct");
            }

            for (int i = 0; i < sortArray.Length - 1; i++)
            {
                if (sortArray[i + 1] != sortArray[i] + 1)
                {
                    return sortArray[i] + 1;
                }
            }
            return -1;
        }

        public static int FindMissingNumberWithSum(int[] array)
        {
            if (array.Length == 0 || array.Length == 1)
            {
                throw new ArgumentException("Size of array is zero");
            }
            int minElement = array[0];
            int maxElement = array[0];
            for (int i = 0; i <= array.Length - 1; i++)
            {
                if (array[i] < minElement)
                    minElement = array[i];
                if (array[i] > maxElement)
                    maxElement = array[i];
            }
            int sumOfElementInArray = 0;
            for (int i = 0; i <= array.Length - 1; i++)
            {
                sumOfElementInArray += array[i];
            }

            int sumElementStartToEnd = 0;
            for (int i = minElement; i <= maxElement; i++)
            {
                sumElementStartToEnd += i;
            }

            return sumElementStartToEnd - sumOfElementInArray;
        }
    }
}