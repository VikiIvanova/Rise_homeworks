﻿using DataStructureTasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestsDataStructures
{
    [TestClass]
    public class UnitTestShuntingYardAndRPN
    {
        [TestMethod]
        public void TestRPNCalculator()
        {
            string input1 = "5";
            string input2 = "1 2 +";
            string input3 = "1 2 + 3 4 + *";
            Tokenizer tokenizer1 = new Tokenizer(new StringReader(input1));
            Tokenizer tokenizer2 = new Tokenizer(new StringReader(input2));
            Tokenizer tokenizer3 = new Tokenizer(new StringReader(input3));

            Assert.AreEqual(5, ShuntingYard.EvalRPN(tokenizer1));
            Assert.AreEqual(3, ShuntingYard.EvalRPN(tokenizer2));
            Assert.AreEqual(21, ShuntingYard.EvalRPN(tokenizer3));
        }

        [TestMethod]
        public void TestShuntingYard()
        {
            string input1 = "5";
            string input2 = "1 + 2";
            string input3 = "(1 + 2 * 3) - (4 + 6)";
            Tokenizer tokenizer1 = new Tokenizer(new StringReader(input1));
            Tokenizer tokenizer2 = new Tokenizer(new StringReader(input2));
            Tokenizer tokenizer3 = new Tokenizer(new StringReader(input3));

            string rpns1 = ShuntingYard.ConvertToRPN(tokenizer1);
            string rpns2 = ShuntingYard.ConvertToRPN(tokenizer2);
            string rpns3 = ShuntingYard.ConvertToRPN(tokenizer3);

            Tokenizer t1 = new Tokenizer(new StringReader(rpns1));
            Tokenizer t2 = new Tokenizer(new StringReader(rpns2));
            Tokenizer t3 = new Tokenizer(new StringReader(rpns3));

            Assert.AreEqual(5, ShuntingYard.EvalRPN(t1));
            Assert.AreEqual(3, ShuntingYard.EvalRPN(t2));
            Assert.AreEqual(-3, ShuntingYard.EvalRPN(t3));
        }
    }
}
