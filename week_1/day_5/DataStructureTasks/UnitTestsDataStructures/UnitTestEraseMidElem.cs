using DataStructureTasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestsDataStructures
{
    [TestClass]
    public class UnitTestEraseElement
    {
        [TestMethod]
        public void TestEraseElement_WhenArrayWithEvenElement()
        {
            LinkedList llist = new LinkedList();
            llist.push(5);
            llist.push(4);
            llist.push(3);
            llist.push(2);
            llist.push(1);
            LinkedList expected = new LinkedList();
            expected.push(5);
            expected.push(4);
            expected.push(2);
            expected.push(1);

            LinkedList actual = llist.eraseMiddleElement();

            Node expectedNode = expected.head;
            Node actualNode = actual.head;

            while (expectedNode != null && actualNode != null)
            {
                Assert.AreEqual(expectedNode.data, actualNode.data);
                expectedNode = expectedNode.next;
                actualNode = actualNode.next;
            }

            Assert.AreEqual(expectedNode, actualNode);
        }
    }
}