﻿using DataStructureTasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestsDataStructures
{
    [TestClass]
    public class UnitTestExpandExp
    {
        [TestMethod]
        public void TestExpandExp_WhenInputIsCorrect()
        {
            string input = "AB3(DC)2(F)2(E3(G))";
            string expected = "ABDCDCDCFFEGGGEGGG";
            string actual = ExpandExpression.ExpandExp(input);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestExpandExp_WhenInputIsNotCorrect()
        {
            string input2 = "AB3DC)2(F)2(E3(G))";
            Assert.ThrowsException<ArgumentException>(() => ExpandExpression.ExpandExp(input2));
        }

        [TestMethod]
        public void TestExpandExp_WhenInputIsEmpty()
        {
            string input3 = "";
            string expected = "";
            string actual = ExpandExpression.ExpandExp(input3);
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestExpandExp_WhenInputIsNoLetter()
        {
            string input4 = "3(2)(4)5(6)";
            Assert.ThrowsException<ArgumentException>(() => ExpandExpression.ExpandExp(input4));
        }

        [TestMethod]
        public void TestExpandExp_WhenInputIsNestedExpressions()
        {
            string input5 = "A3(2(3)4)B";
            Assert.ThrowsException<ArgumentException>(() => ExpandExpression.ExpandExp(input5));
        }

        [TestMethod]
        public void TestExpandExpWithStack_WhenInputIsCorrect()
        {
            string input = "AB3(DC)2(F)2(E3(G))";
            string expected = "ABDCDCDCFFEGGGEGGG";
            string actual = ExpandExpressionWithStack.ExpandExpWithStacks(input);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestExpandExpWithStack_WhenInputIsNotCorrect()
        {
            string input2 = "AB3DC)2(F)2(E3(G))";
            Assert.ThrowsException<ArgumentException>(() => ExpandExpressionWithStack.ExpandExpWithStacks(input2));
        }

        [TestMethod]
        public void TestExpandExpWithStack_WhenInputIsEmpty()
        {
            string input3 = "";
            Assert.ThrowsException<ArgumentException>(() => ExpandExpressionWithStack.ExpandExpWithStacks(input3));
        }
        [TestMethod]
        public void TestExpandExpWithStack_WhenInputIsNoLetter()
        {
            string input4 = "3(2)(4)5(6)";
            Assert.ThrowsException<ArgumentException>(() => ExpandExpressionWithStack.ExpandExpWithStacks(input4));
        }

        [TestMethod]
        public void TestExpandExpWithStack_WhenInputIsNestedExpressions()
        {
            string input5 = "A3(2(3)4)B";
            Assert.ThrowsException<ArgumentException>(() => ExpandExpressionWithStack.ExpandExpWithStacks(input5));
        }
    }
}
