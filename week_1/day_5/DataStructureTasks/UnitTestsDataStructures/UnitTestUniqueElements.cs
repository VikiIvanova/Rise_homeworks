using DataStructureTasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace UnitTestsDataStructures
{
    [TestClass]
    public class UnitTestUniqueElements
    {
        [TestMethod]
        public void TestUniqueElem_WhenInputIsCorrect()
        {
            List<int> list = new List<int>() { 2, 3, 5, 2, 5, 6 };
            List<int> expected = new List<int>() { 2, 3, 5, 6 };
            List<int> actual = UniqueElement.ArrayWithUniqueElem(list);
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestUniqueElem_WhenInputWithEqualElement()
        {
            List<int> list = new List<int>() { 2, 2, 2, 2, 2, 2 };
            List<int> expected = new List<int>() { 2 };
            List<int> actual = UniqueElement.ArrayWithUniqueElem(list);
            CollectionAssert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestUniqueElem_WhenInputWithOneElement()
        {
            List<int> list = new List<int>() {2};
            List<int> expected = new List<int>() { 2 };
            List<int> actual = UniqueElement.ArrayWithUniqueElem(list);
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestUniqueElem_WhenInputWithZeroElement()
        {
            List<int> list = new List<int>() { };
            Assert.ThrowsException<ArgumentException>(() => UniqueElement.ArrayWithUniqueElem(list));
        }

        [TestMethod]
        public void TestUniqueElem_WhenInputWithNegativeElement()
        {
            List<int> list = new List<int>() { -2, -3, -5, -2, 2, 3, -5, 2};
            List<int> expected = new List<int>() { -5, -3, -2, 2, 3};
            List<int> actual = UniqueElement.ArrayWithUniqueElem(list);
            CollectionAssert.AreEqual(expected, actual);
        }
    }
}