﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructureTasks
{
    public class UniqueElement
    {
        public static List<int> SortArray(List<int> array, int leftIndex, int rightIndex)
        {
            var i = leftIndex;
            var j = rightIndex;
            var pivot = array[leftIndex];

            while (i <= j)
            {
                while (array[i] < pivot)
                {
                    i++;
                }
                while (array[j] > pivot)
                {
                    j--;
                }
                if (i <= j)
                {
                    int temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                    i++;
                    j--;
                }
            }
            if (leftIndex < j)
            {
                SortArray(array, leftIndex, j);
            }
            if (i < rightIndex)
            {
                SortArray(array, i, rightIndex);
            }

            return array;
        }

        public static List<int> ArrayWithUniqueElem(List<int> list)
        {
            if(list.Count == 0)
            {
                throw new ArgumentException("Size of array is zero!");
            }
            SortArray(list, 0, list.Count - 1);
            for (int i = 0; i < list.Count - 1; ++i)
            {
                if (list[i] == list[i + 1])
                {
                    list.RemoveAt(i + 1);
                    i--;
                }
            }
            return list;
        }
    }
}
