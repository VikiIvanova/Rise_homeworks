﻿using System;

namespace DataStructureTasks
{
    public class Node
    {
        public int data;
        public Node next;

        public Node(int d)
        {
            data = d;
            next = null;
        }
    }

    public class LinkedList
    {
        Node head;

        public LinkedList()
        {
            head = null;
        }

        public void push(int new_data)
        {
            Node new_node = new Node(new_data);
            new_node.next = head;
            head = new_node;
        }

        public void printList()
        {
            Node tNode = head;
            while (tNode != null)
            {
                Console.Write(tNode.data + " ");
                tNode = tNode.next;
            }
            Console.WriteLine();
        }

        public bool eraseMiddleElement()
        {
            if (head == null || head.next == null)
            {
                return true;
            }

            Node slow = head;
            Node fast = head;
            Node prev = null;

            while (fast != null && fast.next != null)
            {
                fast = fast.next.next;
                prev = slow;
                slow = slow.next;
            }

            prev.next = slow.next;

            return true;
        }
    }
}
