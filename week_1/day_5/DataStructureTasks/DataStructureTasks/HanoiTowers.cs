﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

using System;
using System.Collections.Generic;

public class HanoiTowers
{
    static Stack<int> Source = new Stack<int>();
    static Stack<int> Buffer = new Stack<int>();
    static Stack<int> Destination = new Stack<int>();
    static int n = 4;

    static void move(Stack<int> src, Stack<int> des)
    {
        des.Push(src.Pop());
    }

    static char char_from_tower(int level, Stack<int> t)
    {
        char ret;
        if (t.Count == level)
        {
            ret = (char)(t.Peek() + '0');
            t.Pop();
        }
        else
        {
            ret = ' ';
        }
        return ret;
    }

    static void print_current(Stack<int> A, Stack<int> B, Stack<int> C)
    {
        char a, b, c;
        Console.WriteLine("current towers : ");
        for (int i = n; i > 0; i--)
        {
            a = char_from_tower(i, A);
            b = char_from_tower(i, B);
            c = char_from_tower(i, C);
            Console.WriteLine("| " + a + "| | " + b + "| | " + c + "|");
        }
    }

    static void solve_Hanoi(Stack<int> A, Stack<int> C, Stack<int> B, int step)
    {
        if (step == 1)
        {
            move(A, C);
            print_current(Source, Buffer, Destination);
        }
        else
        {
            solve_Hanoi(A, B, C, step - 1);

            move(A, C);
            print_current(Source, Buffer, Destination);

            solve_Hanoi(B, C, A, step - 1);
        }
    }


    static void Main(string[] args)
    {
        for (int i = n; i > 0; --i)
        {
            Source.Push(i);
        }
        print_current(Source, Buffer, Destination);


        solve_Hanoi(Source, Destination, Buffer, Source.Count);

        Console.WriteLine("\n\nresult in C ");
        for (int i = Destination.Count; i > 0; i--)
        {
            Console.WriteLine("| " + Destination.Peek() + "|");
            Console.WriteLine("|__|");
            Destination.Pop();
        }
    }
}

