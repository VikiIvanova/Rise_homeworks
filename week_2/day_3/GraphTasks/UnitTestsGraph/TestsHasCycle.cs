using GraphTasks;
namespace UnitTestsGraph
{
    [TestClass]
    public class TestsHasCycle
    {
        [TestMethod]
        public void TestHasCycle_ReturnTrue()
        {  
            var cycle = new CycleDetector();

            var graph = new MatrixGraph(5);
            graph.AddEdge(1, 0);
            graph.AddEdge(0, 2);
            graph.AddEdge(2, 1);
            graph.AddEdge(0, 3);
            graph.AddEdge(3, 4);

            bool expected = true;
            bool actual = cycle.HasCycle(0, graph);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestHasCycle_ReturnFalse()
        {
            var cycle = new CycleDetector();

            var graph = new MatrixGraph(5);
            graph.AddEdge(0, 1);
            graph.AddEdge(1, 2);
            graph.AddEdge(3, 4);
            graph.AddEdge(2, 3);

            bool expected = false;
            bool actual = cycle.HasCycle(0, graph);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestHasCycle_WhenGraphIsEmpty()
        {
            var cycle = new CycleDetector();

            var graph = new MatrixGraph(0);

            bool expected = false;
            bool actual = cycle.HasCycle(0, graph);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestHasCycle_WhenGraphHasOneVertex()
        {
            var cycle = new CycleDetector();

            var graph = new MatrixGraph(1);

            bool expected = false;
            bool actual = cycle.HasCycle(0, graph);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestHasCycle_WhenGraphHasTwoVertices()
        {
            var cycle = new CycleDetector();

            var graph = new MatrixGraph(2);
            graph.AddEdge(0, 1);

            bool expected = false;
            bool actual = cycle.HasCycle(0, graph);
            Assert.AreEqual(expected, actual);
        }
    }
}