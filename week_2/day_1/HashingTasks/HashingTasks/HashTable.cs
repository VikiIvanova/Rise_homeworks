﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace HashingTasks
{
    public class HashTable<TKey, TValue>
    {
        private const int capacity = 32;

        private LinkedList<KeyValuePair<TKey, TValue>>[] items;

        public HashTable()
        {
            items = new LinkedList<KeyValuePair<TKey, TValue>>[capacity];
        }

        public void Add(TKey key, TValue value)
        {
            int index = GetIndex(key);
            if (items[index] == null)
            {
                items[index] = new LinkedList<KeyValuePair<TKey, TValue>>();
            }

            foreach(var item in items[index])
            {
                if(item.Key.Equals(key))
                {
                    throw new ArgumentException("An element with the same key already exists in the hash table");

                }
            }

            items[index].AddLast(new KeyValuePair<TKey, TValue>(key, value));
        }

        public bool Remove(TKey key)
        {
            int index = GetIndex(key);
            if (items[index] != null)
            {
                var current = items[index].First;
                while (current != null)
                {
                    if (current.Value.Key.Equals(key))
                    {
                        items[index].Remove(current);
                        return true;
                    }
                    current = current.Next;
                }
            }
            return false;
        }


        public bool TryGetValue(TKey key, out TValue value)
        {
            int index = GetIndex(key);
            if (items[index] != null)
            {
                var current = items[index].First;
                while(current != null)
                {
                    if(current.Value.Key.Equals(key))
                    {
                        value = current.Value.Value;
                        return true;
                    }
                    current = current.Next;
                }
            }
            value = default(TValue);
            return false;
        }

        private int GetIndex(TKey key)
        {
            int hashCode = key.GetHashCode();
            int index = hashCode % items.Length;

            return index;
        }

        public bool Contains(TKey key)
        {
            int index = GetIndex(key);

            if (items[index] == null)
            {
                return false;
            }

            foreach (var node in items[index])
            {
                if (node.Key.Equals(key))
                {
                    return true;
                }
            }

            return false;
        }


    }
}
