﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashingTasks
{
    public class NonRepeatingCharacters
    {
        public static Dictionary<char, int> addElementsInDictionary(string str)
        {
            Dictionary<char, int> characters = new Dictionary<char, int>();
            int i = 0;
            while (i < str.Length)
            {
                if (characters.ContainsKey(str[i]))
                {
                    characters[str[i]]++;
                }
                else
                {
                    characters.Add(str[i], 1);
                }
                i++;
            }
            return characters;
        }
        public static List<char> FindAllNonRepeating(string str)
        {
            List<char> elements = new List<char>();
            Dictionary<char, int> characters = addElementsInDictionary(str);
            foreach (KeyValuePair<char, int> kvp in characters)
            {
                if(kvp.Value < 2)
                {
                    elements.Add(kvp.Key);
                }
            }
            return elements;
        }

        public static int indexOnFirstNonRepeatingCharacter(string str)
        {
            Dictionary<char, int> characters = addElementsInDictionary(str);
            char firstOccurance = characters.Where(x => x.Value == 1).Select(x => x.Key).FirstOrDefault();
            return str.IndexOf(firstOccurance);
           
        }
    }
}
