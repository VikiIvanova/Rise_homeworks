﻿using HashingTasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace HashingTasks
{
    public class Anagrams
    {
        public static Dictionary<string, List<string>> groupByAnagrams(string[] listOfWords)
        {
            Dictionary<string, List<string>> anagrams = new Dictionary<string, List<string>>();

            foreach (string word in listOfWords)
            {
                char[] chars = word.ToCharArray();
                Array.Sort(chars);
                string sortedWord = new string(chars);

                if (!anagrams.ContainsKey(sortedWord))
                {
                    anagrams[sortedWord] = new List<string>();
                }

                anagrams[sortedWord].Add(word);
            }
            return anagrams; 
        }
    }
}
