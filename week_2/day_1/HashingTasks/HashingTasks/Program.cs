﻿using HashingTasks;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Xml.Linq;
using System;
using System.Collections.Generic;

namespace DataStructure
{

    public class Program
    {
        public static void Main(string[] args)
        {
            //task 1 
            //function with example data;
            Dictionary<string, string> carNumberToOwner = CarsAndOwners.createDictionaryOfCars();

            if (carNumberToOwner.ContainsKey("B3333BB"))
            {
                Console.WriteLine(carNumberToOwner["B3333BB"]);
            }

            var program = new CarsAndOwners();
            program.FindOwnerWithMoreCars(carNumberToOwner);

            Console.WriteLine("-----------------------------");

            //task 5
            string[] words = { "race", "care", "acre", "arc", "car" };
            Dictionary<string, List<string>> anagrams = Anagrams.groupByAnagrams(words);
            foreach (KeyValuePair<string, List<string>> entry in anagrams)
            {
                Console.WriteLine("Anagrams of " + entry.Value[0] + ": " + string.Join(", ", entry.Value));
            }
        }
    }
}