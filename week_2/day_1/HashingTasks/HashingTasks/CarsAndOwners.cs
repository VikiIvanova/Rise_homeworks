﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace HashingTasks
{
    public class CarsAndOwners
    {
        public static Dictionary<string, string> createDictionaryOfCars()
        {
            Dictionary<string, string> carNumberToOwner = new Dictionary<string, string>();
            carNumberToOwner.Add("EB3826AC", "Viktoriya Ivanova");
            carNumberToOwner.Add("EB0575BB", "Viktoriya Ivanova");
            carNumberToOwner.Add("EB0755AX", "Viktoriya Ivanova");
            carNumberToOwner.Add("CB5839TT", "Dimitrina Dimitrova");
            carNumberToOwner.Add("CB1234TT", "Ivan Ivanov");
            carNumberToOwner.Add("A4154HH", "Nikola Nikolov");
            carNumberToOwner.Add("B3333BB", "Timo Timov");

            return carNumberToOwner;
        }

        public void FindOwnerWithMoreCars(Dictionary<string, string> carsWithOwners)
        {
            var ownersWithMoreCars = carsWithOwners.GroupBy(x => x.Value).Where(x => x.Count() > 1);
            foreach(var item in ownersWithMoreCars)
            {
                Console.WriteLine(item.Key);
            }
        }
    }
}
