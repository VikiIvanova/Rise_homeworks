﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HashingTasks;
using System.Reflection.Metadata;

namespace UnitTestsHashing
{
    [TestClass]
    public class TestsSpellChecker
    {
        [TestMethod]
        public void TestsSpellCheck_WhenInputIsCorrect()
        {
            HashSet<string> inputDictionary = new HashSet<string>() { "hello", "world", "this", "is", "a", "test" };
            string inputString = "Hello wurld this is a tets";
            List<string> expected = new List<string>() { "wurld", "tets" };
            List<string> actual = SpellChecker.SpellCheck(inputDictionary, inputString);
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestsSpellCheck_WhenStringIsAbsoluteCorrect()
        {
            HashSet<string> inputDictionary = new HashSet<string>() { "hello", "world", "this", "is", "a", "test" };
            string inputString = "hello world";
            List<string> expected = new List<string>() { };
            List<string> actual = SpellChecker.SpellCheck(inputDictionary, inputString);
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestsSpellCheck_WhenInDictionaryNoElementOfString()
        {
            HashSet<string> inputDictionary = new HashSet<string>() { "hello", "world", "this", "is", "a", "test" };
            string inputString = "Maria like oranges";
            List<string> expected = new List<string>() { "Maria", "like", "oranges" };
            List<string> actual = SpellChecker.SpellCheck(inputDictionary, inputString);
            CollectionAssert.AreEqual(expected, actual);
        }
    }
}
