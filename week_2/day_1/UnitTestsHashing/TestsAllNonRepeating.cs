using HashingTasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace UnitTestsHashing
{
    [TestClass]
    public class TestsAllNonRepeating
    {
        [TestMethod]
        public void TestAllNonRepeating_WhenInputIsCorrect()
        {
            string str = "Lorem ipsum is simply dummy text";
            List<char> expected = new List<char> { 'L', 'o', 'r', 'l', 'd', 'x' };
            List<char> actual = NonRepeatingCharacters.FindAllNonRepeating(str);
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestAllNonRepeating_WhenInputEmptyString()
        {
            string str = "";
            List<char> expected = new List<char> {};
            List<char> actual = NonRepeatingCharacters.FindAllNonRepeating(str);
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestAllNonRepeating_WhenInInputNoUniqueElement()
        {
            string str = "abccba";
            List<char> expected = new List<char> { };
            List<char> actual = NonRepeatingCharacters.FindAllNonRepeating(str);
            CollectionAssert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void indexOnFirstNonRepeating_WhenInputIsCorrect()
        {
            string str = "Lorem ipsum is simply dummy text";
            int expected = 0;
            int actual = NonRepeatingCharacters.indexOnFirstNonRepeatingCharacter(str);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void indexOnFirstNonRepeating_WhenInputEmptyString()
        {
            string str = "";
            int expected = -1;
            int actual = NonRepeatingCharacters.indexOnFirstNonRepeatingCharacter(str);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void indexOnFirstNonRepeating_WhenInInputNoUniqueElement()
        {
            string str = "abccba";
            int expected = -1;
            int actual = NonRepeatingCharacters.indexOnFirstNonRepeatingCharacter(str);
            Assert.AreEqual(expected, actual);
        }
    }
}