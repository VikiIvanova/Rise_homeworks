﻿using HashingTasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HashingTasks;

namespace UnitTestsHashing
{
    [TestClass]
    public class TestsHashTable
    {
        [TestMethod]
        public void Insert_AddsElementToHashTable()
        {
            var hashTable = new HashTable<int, string>();
            int key = 1;
            string value = "value";

            hashTable.Add(key, value);

            Assert.IsTrue(hashTable.Contains(key));
        }

        [TestMethod]
        public void Delete_RemovesElementFromHashTable()
        {
            var hashTable = new HashTable<int, string>();
            int key = 1;
            string value = "value";
            hashTable.Add(key, value);

            hashTable.Remove(key);

            Assert.IsFalse(hashTable.Contains(key));
        }
    }
}
