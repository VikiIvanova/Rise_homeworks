﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreeTasks;
namespace UnitTestsTrees
{
    [TestClass]
    public class TestsHeapSort
    {
        [TestMethod]
        public void TestHeapSort()
        {
            var HeapSort = new HeapSort();

            List<int> input1 = new List<int>() { 2, 6, 4, 8, 9, 3, 7, 1, 10 };
            List<int> result1 = new List<int>() { 1, 2, 3, 4, 6, 7, 8, 9, 10 };
            HeapSort.sortHeap(input1);
            CollectionAssert.AreEqual(input1, result1);

            List<int> input2 = new List<int>() { -2, 6, -4, 8, -9, 3, -7, 1, -10 };
            List<int> result2 = new List<int>() { -10, -9, -7, -4, -2, 1, 3, 6, 8 };
            HeapSort.sortHeap(input2);
            CollectionAssert.AreEqual(input2, result2);

        }
    }
}
