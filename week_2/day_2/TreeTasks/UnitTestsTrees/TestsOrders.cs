using TreeTasks;
namespace UnitTestsTrees
{
    [TestClass]
    public class TestsOrders
    {
        public BinaryTreeNode<int> ConstructTree()
        {
            BinaryTreeNode<int> node9 = new BinaryTreeNode<int>(76, null, null);
            BinaryTreeNode<int> node8 = new BinaryTreeNode<int>(61, null, null);
            BinaryTreeNode<int> node7 = new BinaryTreeNode<int>(35, null, null);

            BinaryTreeNode<int> node6 = new BinaryTreeNode<int>(69, node8, node9);
            BinaryTreeNode<int> node5 = new BinaryTreeNode<int>(38, node7, null);
            BinaryTreeNode<int> node4 = new BinaryTreeNode<int>(24, null, null);

            BinaryTreeNode<int> node3 = new BinaryTreeNode<int>(52, node5, node6);
            BinaryTreeNode<int> node2 = new BinaryTreeNode<int>(11, null, node4);
            BinaryTreeNode<int> node1 = new BinaryTreeNode<int>(25, node2, node3);

            return node1;
        }
        [TestMethod]
        public void TestPreOrders_WhenInputWithIntegers()
        {
            var Orders = new Orders();
            BinaryTreeNode<int> tree = ConstructTree();
            List<int> actual = new List<int>();
            List<int> expected = new List<int>() { 25, 11, 24, 52, 38, 35, 69, 61, 76 };
            Orders.PreOrders(tree, actual);

            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestPostOrders_WhenInputWithIntegers()
        {
            var Orders = new Orders();
            BinaryTreeNode<int> tree = ConstructTree();
            List<int> actual = new List<int>();
            List<int> expected = new List<int>() { 24, 11, 35, 38, 61, 76, 69, 52, 25 };
            Orders.PostOrders(tree, actual);

            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestInOrders_WhenInputWithIntegers()
        {
            var Orders = new Orders();
            BinaryTreeNode<int> tree = ConstructTree();
            List<int> actual = new List<int>();
            List<int> expected = new List<int>() { 11, 24, 25, 35, 38, 52, 61, 69, 76 };
            Orders.InOrders(tree, actual);

            CollectionAssert.AreEqual(expected, actual);
        }
    }
}