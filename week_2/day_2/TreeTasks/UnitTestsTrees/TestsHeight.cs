global using Microsoft.VisualStudio.TestTools.UnitTesting;
using TreeTasks;

namespace UnitTestsTrees
{
    [TestClass]
    public class TestsHeight
    {
        public BinaryTreeNode<int> ConstructTree()
        {
            BinaryTreeNode<int> node9 = new BinaryTreeNode<int>(76, null, null);
            BinaryTreeNode<int> node8 = new BinaryTreeNode<int>(61, null, null);
            BinaryTreeNode<int> node7 = new BinaryTreeNode<int>(35, null, null);

            BinaryTreeNode<int> node6 = new BinaryTreeNode<int>(69, node8, node9);
            BinaryTreeNode<int> node5 = new BinaryTreeNode<int>(38, node7, null);
            BinaryTreeNode<int> node4 = new BinaryTreeNode<int>(24, null, null);

            BinaryTreeNode<int> node3 = new BinaryTreeNode<int>(52, node5, node6);
            BinaryTreeNode<int> node2 = new BinaryTreeNode<int>(11, null, node4);
            BinaryTreeNode<int> node1 = new BinaryTreeNode<int>(25, node2, node3);

            return node1;
        }
        [TestMethod]
        public void TestHeight_WhenInputWithIntegers()
        {
            var Height = new Height();
            BinaryTreeNode<int> tree = ConstructTree();
            int actual = 3;
            int expected = Height.HeightBinaryTree(tree);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestHeight_WhenInputIsEmptyTree()
        {
            var Height = new Height();
            BinaryTreeNode<int> tree = null;
            int actual = 0;
            int expected = Height.HeightBinaryTree(tree);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestHeight_WhenInputIsTreeWithOneElement()
        {
            var Height = new Height();
            BinaryTreeNode<int> tree = new BinaryTreeNode<int>(5, null, null);
            int actual = 0;
            int expected = Height.HeightBinaryTree(tree);
            Assert.AreEqual(expected, actual);
        }
    }
}