﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fibonacci
{
    public class FindNumberWithFibNeighbours
    {
        public static int FindNumberWithFibonacciNeighbors(int[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (i > 0 && j > 0 && i < matrix.GetLength(0) - 1 && j < matrix.GetLength(1) - 1)
                    {
                        int a = matrix[i - 1, j];
                        int b = matrix[i, j - 1];
                        int c = matrix[i, j + 1];
                        int d = matrix[i + 1, j];

                        if (IsFibonacciSequence(a, b, matrix[i, j]) || IsFibonacciSequence(b, matrix[i, j], c) || IsFibonacciSequence(matrix[i, j], c, d))
                        {
                            return matrix[i, j];
                        }
                    }
                }
            }

            return -1;
        }

        public static bool IsFibonacciSequence(int a, int b, int c)
        {
            return a + b == c && IsPerfectSquare(5 * c * c + 4) || IsPerfectSquare(5 * c * c - 4);
        }

        public static bool IsPerfectSquare(int n)
        {
            int sqrt = (int)Math.Sqrt(n);
            return sqrt * sqrt == n;
        }

        public static void Main(string[] args)
        {

        }
    }
}
