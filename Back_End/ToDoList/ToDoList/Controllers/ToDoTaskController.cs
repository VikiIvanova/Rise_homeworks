﻿using Microsoft.AspNetCore.Mvc;
using ToDoList.Models;
using ToDoList.Service;

namespace ToDoList.Controllers
{
    public class ToDoTaskController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            List<ToDoTask> tasks = new List<ToDoTask>();

            using (ToDoListService toDoListService = new ToDoListService())
            {
                tasks = toDoListService.GetAllTasks();
            }

            return View(tasks);

        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(ToDoTask task)
        {
            if (task == null)
            {
                return BadRequest();
            }
            ToDoTask taskToAdd = new ToDoTask();
            taskToAdd.Name = task.Name;
            taskToAdd.CreatedOn = task.CreatedOn;
            taskToAdd.Description = task.Description;
            taskToAdd.Done = false;
            taskToAdd.Assignment = task.Assignment;

            using (ToDoListService toDoListService = new ToDoListService())
            {
                toDoListService.CreateToDoTask((taskToAdd));
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            ToDoTask taskToEdit;
            using (ToDoListService toDoListService = new ToDoListService())
            {
                taskToEdit = toDoListService.GetToDoTask(id);
            }

            if (taskToEdit == null)
            {
                return NotFound();
            }

            return View(taskToEdit);

        }
        [HttpPost]
        public IActionResult Edit(ToDoTask task)
        {
            if (task == null)
            {
                return BadRequest();
            }

            bool result = false;
            using (ToDoListService toDoListService = new ToDoListService())
            {
                result = toDoListService.TryEditTask(task);
            }
            if (result)
            {
                return RedirectToAction("Index");
            }

            return BadRequest();
        }

        [HttpGet]
        public IActionResult Details(int id)
        {
            ToDoTask taskToDetails = new ToDoTask();
            using (ToDoListService toDoListService = new ToDoListService())
            {
                taskToDetails = toDoListService.GetToDoTask(id);
            }
            if (taskToDetails == null)
            {
                return NotFound();
            }
            return View(taskToDetails);
        }
        [HttpGet]
        public IActionResult Delete(int id)
        {
            using (ToDoListService toDoListService = new ToDoListService())
            {
                try
                {
                    toDoListService.DeleteTask(id);
                }
                catch (InvalidOperationException e)
                {
                    return NotFound(e);
                }
            }
            return RedirectToAction("Index");
        }
    }
}
