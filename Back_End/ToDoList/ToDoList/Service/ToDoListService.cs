﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using ToDoList.Models;

namespace ToDoList.Service
{
    public class ToDoListService : IDisposable
    {
        public void Dispose()
        {

        }

        [HttpGet]
        public List<ToDoTask> GetAllTasks()
        {
            List<ToDoTask> tasks = new List<ToDoTask>();
            using (ToDoTaskContext context = new ToDoTaskContext())
            {
                tasks = (from task in context.ToDoTasks
                         join assignment in context.Assignments
                             on task.AssignmentId equals assignment.AssignmentId
                         select new ToDoTask
                         {
                             TaskId = task.TaskId,
                             Name = task.Name,
                             CreatedOn = task.CreatedOn,
                             Done = task.Done,
                             Description = task.Description,
                             Assignment = assignment
                         }).ToList();

            }
            return tasks;
        }

        [HttpPost]
        public void CreateToDoTask(ToDoTask taskToAdd)
        {
            using (ToDoTaskContext context = new ToDoTaskContext())
            {
                context.ToDoTasks.Add(taskToAdd);
                context.SaveChanges();

            }
        }

        [HttpGet]
        public ToDoTask GetToDoTask(int id)
        {
            ToDoTask taskToEdit = new ToDoTask();
            using (ToDoTaskContext context = new ToDoTaskContext())
            {
                taskToEdit = (from task in context.ToDoTasks
                              join assignment in context.Assignments
                                  on task.AssignmentId equals assignment.AssignmentId
                              select new ToDoTask
                              {
                                  TaskId = task.TaskId,
                                  Name = task.Name,
                                  CreatedOn = task.CreatedOn,
                                  Done = task.Done,
                                  Description = task.Description,
                                  Assignment = assignment
                              }).FirstOrDefault(t => t.TaskId == id);
            }

            return taskToEdit;
        }

        [HttpPost]
        public bool TryEditTask(ToDoTask taskToEdit)
        {
            using (ToDoTaskContext context = new ToDoTaskContext())
            {
                var modifiedTask = context.ToDoTasks.FirstOrDefault(t => t.TaskId == taskToEdit.TaskId);

                modifiedTask.Name = taskToEdit.Name;
                modifiedTask.CreatedOn = taskToEdit.CreatedOn;
                modifiedTask.Description = taskToEdit.Description;
                modifiedTask.Done = taskToEdit.Done;

                Assignment assignment = context.Assignments.FirstOrDefault(t => t.AssignmentId == modifiedTask.AssignmentId);

                assignment.CreatedBy = taskToEdit.Assignment.CreatedBy;
                assignment.AssignedTo = taskToEdit.Assignment.AssignedTo;
                modifiedTask.Assignment = assignment;
                int result = context.SaveChanges();

                return result > 0;
            }
        }

        [HttpGet]
        public void DeleteTask(int id)
        {
            using (ToDoTaskContext context = new ToDoTaskContext())
            {
                ToDoTask task = context.ToDoTasks.FirstOrDefault(x => x.TaskId == id);
                if (task == null)
                {
                    throw new InvalidOperationException("Task not found.");
                }
                context.ToDoTasks.Remove(task);
                Assignment assignment = context.Assignments.FirstOrDefault(x => x.AssignmentId == task.AssignmentId);
               
                if (assignment == null)
                {
                    throw new InvalidOperationException("Assignment not found.");
                }
                context.Assignments.Remove(assignment);
                context.SaveChanges();
            }
        }
    }
}
