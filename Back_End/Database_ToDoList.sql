CREATE DATABASE ToDoTask

USE ToDoTask

CREATE TABLE Assignments 
(
    AssignmentId INT PRIMARY KEY IDENTITY,
    CreatedBy VARCHAR(50) NOT NULL,
    AssignedTo VARCHAR(50) NOT NULL
);

CREATE TABLE ToDoTasks (
    TaskId INT PRIMARY KEY IDENTITY,
    [Name] VARCHAR(50) NOT NULL,
    CreatedOn DATETIME NOT NULL,
    Done BIT NOT NULL DEFAULT 0,
    [Description] VARCHAR(255),
    AssignmentId INT NOT NULL,
    CONSTRAINT FK_ToDoTasks_Assignments FOREIGN KEY (AssignmentId) REFERENCES Assignments(AssignmentId)
);